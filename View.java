package Controle;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class View extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtSalario;
	private JTextField txtSalarioSemPoupanca;
	private JTextField txtCompra;
	private JTextField txtCompraPoupanca;
	private JButton btnSalvar_1;
	private JButton btnSalvar_2;
	private JButton btnSalvar_3;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public View() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAdicionarSalrio = new JLabel("Receita:");
		lblAdicionarSalrio.setBounds(12, 12, 65, 15);
		contentPane.add(lblAdicionarSalrio);

		txtSalario = new JTextField();
		txtSalario.setBounds(74, 10, 84, 19);
		contentPane.add(txtSalario);
		txtSalario.setColumns(10);

		JLabel lblAdicionarSalrioSem = new JLabel("Receita sem reserva:");
		lblAdicionarSalrioSem.setBounds(12, 39, 150, 15);
		contentPane.add(lblAdicionarSalrioSem);

		txtSalarioSemPoupanca = new JTextField();
		txtSalarioSemPoupanca.setBounds(165, 37, 114, 19);
		contentPane.add(txtSalarioSemPoupanca);
		txtSalarioSemPoupanca.setColumns(10);

		JLabel lblValorDaCompra = new JLabel("Valor da Compra: ");
		lblValorDaCompra.setBounds(12, 73, 132, 15);
		contentPane.add(lblValorDaCompra);

		txtCompra = new JTextField();
		txtCompra.setBounds(145, 69, 114, 19);
		contentPane.add(txtCompra);
		txtCompra.setColumns(10);

		JLabel lblCompraComPoupana = new JLabel("Utilizar reserva: ");
		lblCompraComPoupana.setBounds(12, 103, 178, 15);
		contentPane.add(lblCompraComPoupana);

		txtCompraPoupanca = new JTextField();
		txtCompraPoupanca.setBounds(132, 101, 114, 19);
		contentPane.add(txtCompraPoupanca);
		txtCompraPoupanca.setColumns(10);

		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Receita.valorInicial = Double.parseDouble(txtSalario.getText());
					Receita.calculaPoupanca();
					Receita.calculaSaldoLiquido();
					Receita.Print();
					txtSalario.setText(null);
				} catch (NumberFormatException nfe) {
					System.err.println("Entre com um valor numérico!");

				}

			}
		});
		btnSalvar.setBounds(170, 12, 78, 15);
		contentPane.add(btnSalvar);

		btnSalvar_1 = new JButton("Salvar");
		btnSalvar_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Receita.adicionaSemPoupanca = Double.parseDouble(txtSalarioSemPoupanca.getText());
					Receita.calculaSemPoupanca();
					Receita.Print();
					txtSalarioSemPoupanca.setText(null);
				} catch (NumberFormatException nfe) {
					System.err.println("Entre com um valor numérico!");

				}
			}
		});
		btnSalvar_1.setBounds(288, 39, 84, 15);
		contentPane.add(btnSalvar_1);

		btnSalvar_2 = new JButton("Salvar");
		btnSalvar_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Receita.debito = Double.parseDouble(txtCompra.getText());
					Receita.calculaDebito();
					Receita.Print();
					txtCompra.setText(null);
				} catch (NumberFormatException nfe) {
					System.err.println("Entre com um valor numérico!");

				}

			}
		});
		btnSalvar_2.setBounds(271, 73, 78, 15);
		contentPane.add(btnSalvar_2);

		btnSalvar_3 = new JButton("Salvar");
		btnSalvar_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Receita.debito = Double.parseDouble(txtCompraPoupanca.getText());
					Receita.adicionaSemPoupanca = Double.parseDouble(txtCompraPoupanca.getText());
					Receita.utilizaPoupanca = (Receita.valorFinal + Receita.valorPoupado);
					Receita.debitoPoupanca();
					Receita.Print();
					txtCompraPoupanca.setText(null);
				} catch (NumberFormatException nfe) {
					System.err.println("Entre com um valor numérico!");

				}

			}
		});
		btnSalvar_3.setBounds(258, 100, 84, 15);
		contentPane.add(btnSalvar_3);
	}
}