package Controle;

public class Receita extends Debito {
	static double valorFinal;
	static double valorInicial = 0;
	static double valorLiquido;
	static double adicionaSaldo;
	static double adicionaSemPoupanca;

	public Receita() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static double getAdicionaSemPoupanca() {
		return adicionaSemPoupanca;
	}

	public static void setAdicionaSemPoupanca(double adicionaSemPoupanca) {
		Receita.adicionaSemPoupanca = adicionaSemPoupanca;
	}

	public static double getAdicionaSaldo() {
		return adicionaSaldo;
	}

	public static void setAdicionaSaldo(double adicionaSaldo) {
		Receita.adicionaSaldo = adicionaSaldo;
	}

	public static double getValorLiquido() {
		return valorLiquido;
	}

	public static void setValorLiquido(double valorLiquido) {
		Receita.valorLiquido = valorLiquido;
	}

	public static double getValorFinal() {
		return valorFinal;
	}

	public static void setValorFinal(double valorFinal) {
		Receita.valorFinal = valorFinal;
	}

	public static double getValorInicial() {
		return valorInicial;
	}

	public static void setValorInicial(double valorInicial) {
		Receita.valorInicial = valorInicial;
	}

	// funções--------------------------------------------------------------------------

	public static void calculaSaldoLiquido() {
		adicionaSaldo = valorInicial - valorPoupanca;
		valorLiquido = valorLiquido + adicionaSaldo;
		valorFinal = valorLiquido;
	}

	public static void calculaSaldoFinal() {
		valorFinal = valorFinal - debito;
	}

	public static void calculaSemPoupanca() {
		valorFinal = valorFinal + adicionaSemPoupanca;
	}

	public static void Print() {
		System.out.println("O saldo atual é: " + valorFinal);
		System.out.println("O valor poupado é: " + valorPoupado);
	}
}
