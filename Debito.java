package Controle;

public abstract class Debito extends Poupanca {
	protected static double preco;
	protected static double debito;
	protected static double utilizaPoupanca;

	public Debito() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static double getUtilizaPoupanca() {
		return utilizaPoupanca;
	}

	public static void setUtilizaPoupanca(double utilizaPoupanca) {
		Debito.utilizaPoupanca = utilizaPoupanca;
	}

	public static double getPreco() {
		return preco;
	}

	public static void setPreco(double preco) {
		Debito.preco = preco;
	}

	// funções--------------------------------------------------------------------------------

	public static void calculaDebito() {
		if (Receita.valorFinal >= Receita.debito) {
			Receita.calculaSaldoFinal();

		} else if (Receita.valorFinal < Receita.debito) {
			System.out.println("a compra não pode ser realizada!");
		}

	}

	public static void debitoPoupanca() {

		if (Receita.utilizaPoupanca >= Receita.debito) {
			if (Receita.debito > Receita.valorFinal) {
				Receita.valorFinal = Receita.utilizaPoupanca;
				Receita.calculaSaldoFinal();
				Receita.valorPoupado = Receita.valorFinal;
				Receita.valorFinal = 0;
				System.out.println("");
			} else if (Receita.debito <= Receita.valorFinal) {
				System.out.println("a compra não pode ser realizada!");
			}
		} else if (Receita.utilizaPoupanca < Receita.debito) {
			System.out.println("a compra não pode ser realizada!");
		}
	}

}
